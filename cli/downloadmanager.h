#ifndef DOWNLOADMANAGER_H
#define DOWNLOADMANAGER_H

#include <QtNetwork>
#include <QtCore>

class DownloadManager: public QObject
{
    Q_OBJECT
public:
    explicit DownloadManager(QObject *parent = nullptr);

    void append(const QUrl &url);
    void append(const QStringList &urls);
    static QString saveFileName(const QUrl &url);

signals:
    /**某个下载项开始*/
    void start(const QUrl &url);
    /**当前下载项进度*/
    void progress(const QUrl &url, qint64 bytesReceived, qint64 bytesTotal);
    /**某个下载项完成*/
    void milestone(const QUrl &url);
    /**所有下载项完成*/
    void finished();
    /**某个下载项发生错误*/
    void error(const QUrl &url);

private slots:
    void startNextDownload();
    void downloadProgress(qint64 bytesReceived, qint64 bytesTotal);
    void downloadFinished();
    void downloadReadyRead();

private:
    bool isHttpRedirect() const;
    void reportRedirect();

    QNetworkAccessManager manager;
    QQueue<QUrl> downloadQueue;
    QNetworkReply *currentDownload = nullptr;
    QFile output;
    QElapsedTimer downloadTimer;

    int downloadedCount = 0;
    int totalCount = 0;

    static const QString TmpSuffix;
};

#endif // DOWNLOADMANAGER_H
