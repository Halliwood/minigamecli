#include "cliwindow.h"
#include "ui_cliwindow.h"

#include <QMouseEvent>
#include <QPainter>
#include <QWebEnginePage>
#include <QWebEngineNewWindowRequest>
#include <QDesktopServices>
#include "ICliWindow.h"

CliWindow::CliWindow(QWidget *parent)
    : WebCliWindow(parent, Qt::FramelessWindowHint)
    , m_ui(new Ui::CliWindow)
{
    m_ui->setupUi(this);

    setAttribute(Qt::WA_TranslucentBackground);

    m_wui.centralwidget = m_ui->centralwidget;
    m_wui.webViewCtn = m_ui->webViewCtn;
    m_wui.progressBar = m_ui->progressBar;
    m_wui.textStatus = m_ui->textStatus;
    setUI(&m_wui, true);

    auto page = m_webView->page();
    connect(page, &QWebEnginePage::newWindowRequested, this, &CliWindow::onNewWindowRequested);
}

CliWindow::~CliWindow()
{
    delete m_ui;
}

void CliWindow::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        m_dragPosition = event->globalPosition().toPoint() - frameGeometry().topLeft();
        event->accept();
    }
}

void CliWindow::mouseMoveEvent(QMouseEvent *event)
{
    if (event->buttons() & Qt::LeftButton) {
        move(event->globalPosition().toPoint() - m_dragPosition);
        event->accept();
    }
}

void CliWindow::paintEvent(QPaintEvent *)
{
}


void CliWindow::on_btnClose_clicked()
{
    QApplication::exit(0);
}


void CliWindow::on_btnMin_clicked()
{
    this->showMinimized();
}

void CliWindow::onNewWindowRequested(QWebEngineNewWindowRequest &request)
{
    QDesktopServices::openUrl(request.requestedUrl());
}

