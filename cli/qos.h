#ifndef QOS_H
#define QOS_H

#include <QNetworkAccessManager>

class Qos
{
public:
    Qos();

    // QOS类型
    enum QosType
    {
        None = 0,
        PAGE_READY = 1,   // 页面ready
        QUERY_OPEN_GAME = 2,   // 微端收到启动unity请求
        QUERY_MIN_START = 3,   // 开始查询最低版本号
        QUERY_MIN_FINISH = 4,   // 完成查询最低版本号
        QUERY_MIN_ERR = 5,   // 最低版本号查询错误
        DOWNLOAD_PFCFG_ERR = 6,   // 平台配置下载错误
        DOWNLOAD_SHELLCFG_ERR = 6,   // 微端配置下载错误
        DOWNLOAD_PKG_START = 10,  // 开始下载zip包
        DOWNLOAD_PKG_FINISH = 11,  // 完成下载zip
        DOWNLOAD_PKG_ERR = 12,  // zip下载错误
        UNZIP_PKG_START = 20,  // 开始解压zip
        UNZIP_PKG_FINISH = 21,  // 完成解压zip
        UNZIP_PKG_ERR = 22,  // zip解压错误
        QUERY_RELINK_START = 30,  // 请求大厅新连接
        QUERY_RELINK_FINISH = 40,  // 新连接完成
        LAUNCH_UNITY_ERR = 41,  // 启动unity错误
        LAUNCH_UNITY_NODX = 42,  // 未安装dx9
        LAUNCH_UNITY_DRIVERERR = 43,  // 显卡驱动错误
    };

    void setQosEnv(const QString &url, const QString &appVer, const QString &osVer);
    void setUID(const QString &uid);
    void report(const QosType &type, const char *data);
    int getStartTime() const;

private:
    QString m_url;
    QString m_uid;
    QString m_appVer;
    QString m_osVer;
    int m_startTime = 0;

    QNetworkAccessManager *m_mgr;
};

#endif // QOS_H
