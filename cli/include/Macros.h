#ifndef MACROS_H
#define MACROS_H

#define EXITCODE_REBOOT  773

#define UNITY_READY_TOLERANCE	3000

#define RES_LOGFILE "log.log"
#define RES_PREVLOGFILE "log_prev.log"

#define DELAY_POP_WINDOW        1000 // 1s后弹出微端

// 配置键值
#define CONFIG_KEY_UPLOAD	"Game/upload"
#define CONFIG_KEY_PID		"Game/pid"
#define CONFIG_KEY_PM		"Game/pm"
#define CONFIG_KEY_PAGE		"Game/page"
#define CONFIG_KEY_JUMP		"Game/jump"
#define CONFIG_KEY_STARTJUMP		"Game/startJump"
#define CONFIG_KEY_PRODUCTNAME	"Game/productName"
#define CONFIG_KEY_PLATFORM "Game/platform"
#define CONFIG_KEY_HOMEPAGE "Game/home"
#define CONFIG_KEY_BBS		"Game/bbs"
#define CONFIG_KEY_REGISTER	"Game/register"
#define CONFIG_KEY_SERVICE	"Game/service"
#define CONFIG_KEY_CHARGE	"Game/charge"
#define CONFIG_KEY_AIRCLIENT "Game/airclient"
#define CONFIG_KEY_INSTSCUCCESS "Game/success"
#define CONFIG_KEY_FIRSTOPEN "Game/firstopen"
#define CONFIG_KEY_PKGNAME	"Game/pkgName"
#define CONFIG_KEY_SHELLCFGURL	"Game/shellcfgURL"
#define CONFIG_KEY_PLATFORMCFGNAME	"Game/pfCfgName"
#define CONFIG_KEY_PLATFORMURL	"Game/pfURL"
#define CONFIG_KEY_PKG		"Game/pkg"
#define CONFIG_KEY_LOG		"Game/log"
#define CONFIG_KEY_QOS		"Game/qos"
#define CONFIG_KEY_TESTKEY	"Game/testkey"
#define CONFIG_KEY_PARAMS	"Game/params"
#define CONFIG_KEY_STRATERGY	"Game/stratergy"
#define CONFIG_KEY_GAMEID	"Game/gameid"
// Box
#define CONFIG_KEY_BOXLOGIN "Box/login"
#define CONFIG_KEY_BOXINSTALLEDPATH "Box/installPath"

// shellcfg.ini
#define CONFIG_KEY_DISABLEXIT	"shell/disableExit"
#define CONFIG_KEY_MINVER	"game/minver"
#define CONFIG_KEY_UPDATEFLAG   "update/updateFlag"
#define CONFIG_KEY_UPDATESHELLMIN   "update/min"
#define CONFIG_KEY_UPDATEPKGNAME   "update/pkgName"
#define CONFIG_KEY_UPDATELATEST   "update/latest"

#define URL_DX9_XP "http://dlx.fygame.com/pcapps/dxwebsetup.exe"
#define URL_DX9_WIN7 "http://dlx.fygame.com/pcapps/DirectX_9_0c_Windows_7_1744606532.exe"
#define URL_DX9_BAIDU "https://www.baidu.com/s?wd=%E5%AE%89%E8%A3%85dx9"
#define URL_XIANKA_BAIDU "https://www.baidu.com/s?wd=%E9%A9%B1%E5%8A%A8%E7%B2%BE%E7%81%B5"

#endif // MACROS_H
