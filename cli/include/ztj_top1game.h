#ifndef ZTJ_TOP1GAME_H
#define ZTJ_TOP1GAME_H

#define GAME_TITLE "帝君無雙"

#define WIN_TITLE_ERROR "錯誤"
#define WIN_TITLE_EXIT "關閉提示"
#define WIN_TITLE_COMP "缺少組件"
#define WIN_TITLE_XIANKA "顯卡驅動錯誤"
#define WIN_TITLE_UPDATE "微端更新"

#define MSG_CRASH "遊戲崩潰啦"
#define MSG_UNZIP_ERROR "遊戲包解壓失敗！"
#define MSG_UPDATE "新微端已經更新完畢，重啟微端即可體驗"
#define MSG_CALL_UNITY_TIP "微端可能已損壞，請重新下載安裝"
#define MSG_DX9_ERROR "檢測到您尚未安裝微軟DirectX9，安裝後即可體驗極致酷炫遊戲效果！"
#define MSG_UNITY_ERROR "獲取顯卡資訊失敗，請升級顯卡驅動或者聯繫客服幫助。"
#define MSG_LOGIN_ERROR "登錄遊戲失敗，請再來一次"
#define MSG_CLEAN_TITLE "清理提示"
#define MSG_CLEAN_TIP "確定清理流覽器緩存?"
#define MSG_CLEAN_FINISH "緩存清理完畢"

#define BTN_LABEL_OK "確定"
#define BTN_LABEL_CANCEL "取消"
#define BTN_LABEL_OFFICAL "官網"
#define BTN_LABEL_RECHARGE "充值"
#define BTN_LABEL_CLEAN "清理"
#define BTN_LABEL_REFRESH "刷新"

#define ICON_MENU_EXIT "退出"

#define DOWNLOAD_PLATCFG "正在載入平臺配置..."
#define DOWNLOAD_SHELLCFG "正在載入微端配置..."
#define DOWNLOAD_GAMEPKG "正在載入遊戲包..."
#define DOWNLOAD_SHELLPKG "正在載入微端更新包..."

#endif // ZTJ_TOP1GAME_H
