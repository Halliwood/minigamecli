#ifndef DEFAULTLANG_H
#define DEFAULTLANG_H

#define GAME_TITLE "斗罗大陆"

#define WIN_TITLE_ERROR "错误"
#define WIN_TITLE_EXIT "关闭提示"
#define WIN_TITLE_COMP "缺少组件"
#define WIN_TITLE_XIANKA "显卡驱动错误"
#define WIN_TITLE_UPDATE "微端更新"

#define MSG_CRASH "游戏崩溃啦"
#define MSG_UNZIP_ERROR "游戏包解压失败！"
#define MSG_UPDATE "新微端已经更新完毕，重启微端即可体验"
#define MSG_CALL_UNITY_TIP "微端可能已损坏，请重新下载安装"
#define MSG_DX9_ERROR "检测到您尚未安装微软DirectX9，安装后即可体验极致酷炫游戏效果！"
#define MSG_UNITY_ERROR "获取显卡信息失败，请升级显卡驱动或者联系客服帮助。"
#define MSG_LOGIN_ERROR "登录游戏失败，请再来一次"
#define MSG_CLEAN_TITLE "清理提示"
#define MSG_CLEAN_TIP "确定清理浏览器缓存?"
#define MSG_CLEAN_FINISH "缓存清理完毕"

#define BTN_LABEL_OK "确定"
#define BTN_LABEL_CANCEL "取消"
#define BTN_LABEL_OFFICAL "官网"
#define BTN_LABEL_RECHARGE "充值"
#define BTN_LABEL_CLEAN "清理"
#define BTN_LABEL_REFRESH "刷新"

#define ICON_MENU_EXIT "退出"

#define DOWNLOAD_PLATCFG "正在加载平台配置..."
#define DOWNLOAD_SHELLCFG "正在加载微端配置..."
#define DOWNLOAD_GAMEPKG "正在加载游戏包..."
#define DOWNLOAD_SHELLPKG "正在加载微端更新包..."

#endif // DEFAULTLANG_H
