#include "cfgmgr.h"

CfgMgr::CfgMgr()
{
}

void CfgMgr::readCommonConfig(const QString &fileName)
{
    commonReader = std::unique_ptr<QSettings>(new QSettings(fileName, QSettings::IniFormat));
}

void CfgMgr::readPlatConfig(const QString &fileName)
{
    platReader = std::unique_ptr<QSettings>(new QSettings(fileName, QSettings::IniFormat));
}

void CfgMgr::readShellConfig(const QString &fileName)
{
    shellReader = std::unique_ptr<QSettings>(new QSettings(fileName, QSettings::IniFormat));
}
