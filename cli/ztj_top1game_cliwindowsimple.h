#ifndef ZTJ_TOP1GAME_CLIWINDOWSIMPLE_H
#define ZTJ_TOP1GAME_CLIWINDOWSIMPLE_H

#include "webcliwindow.h"
namespace Ui {
class Ztj_Top1game_CliWindowSimple;
}

class Ztj_Top1game_CliWindowSimple : public WebCliWindow
{
    Q_OBJECT

public:
    explicit Ztj_Top1game_CliWindowSimple(QWidget *parent = nullptr);
    ~Ztj_Top1game_CliWindowSimple();

private:
    Ui::Ztj_Top1game_CliWindowSimple *m_ui;
    WebCliWindowUI m_wui;
};

#endif // ZTJ_TOP1GAME_CLIWINDOWSIMPLE_H
