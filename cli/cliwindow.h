#ifndef CLIWINDOW_H
#define CLIWINDOW_H

#include <QMainWindow>
#include <QWebEngineView>

#include "webcliwindow.h"

QT_BEGIN_NAMESPACE
namespace Ui { class CliWindow; }
QT_END_NAMESPACE

class CliWindow : public WebCliWindow
{
    Q_OBJECT

public:
    CliWindow(QWidget *parent = nullptr);
    ~CliWindow();

protected:
    void mouseMoveEvent(QMouseEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void paintEvent(QPaintEvent *event) override;

private slots:
    void on_btnClose_clicked();

    void on_btnMin_clicked();

    void onNewWindowRequested(QWebEngineNewWindowRequest &request);

private:
    Ui::CliWindow *m_ui = nullptr;
    WebCliWindowUI m_wui;

    QPoint m_dragPosition;
};
#endif // CLIWINDOW_H
