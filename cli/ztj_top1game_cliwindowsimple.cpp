#include "ztj_top1game_cliwindowsimple.h"
#include "ui_ztj_top1game_cliwindowSimple.h"

Ztj_Top1game_CliWindowSimple::Ztj_Top1game_CliWindowSimple(QWidget *parent) :
    WebCliWindow(parent, Qt::FramelessWindowHint),
    m_ui(new Ui::Ztj_Top1game_CliWindowSimple)
{
    m_ui->setupUi(this);

    setAttribute(Qt::WA_TranslucentBackground);

    m_wui.centralwidget = m_ui->centralwidget;
    m_wui.webViewCtn = m_ui->webViewCtn;
    m_wui.progressBar = m_ui->progressBar;
    m_wui.textStatus = m_ui->textStatus;
    setUI(&m_wui, false);
}

Ztj_Top1game_CliWindowSimple::~Ztj_Top1game_CliWindowSimple()
{
    delete m_ui;
}
