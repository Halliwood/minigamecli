#ifndef CFGMGR_H
#define CFGMGR_H

#include <QSettings>

class CfgMgr
{
public:
    CfgMgr();

    void readCommonConfig(const QString &fileName);
    void readPlatConfig(const QString &fileName);
    void readShellConfig(const QString &fileName);

    template<typename T>
    T readString(const QString &key);

private:
    std::unique_ptr<QSettings> commonReader = nullptr;
    std::unique_ptr<QSettings> platReader = nullptr;
    std::unique_ptr<QSettings> shellReader = nullptr;
};

template<typename T>
T CfgMgr::readString(const QString &key)
{
    if(nullptr != platReader && platReader->contains(key))
    {
        return platReader->value(key).value<T>();
    }
    if(nullptr != commonReader && commonReader->contains(key))
    {
        return commonReader->value(key).value<T>();
    }
    if(nullptr != shellReader && shellReader->contains(key))
    {
        return shellReader->value(key).value<T>();
    }
    return T();
}

#endif // CFGMGR_H
