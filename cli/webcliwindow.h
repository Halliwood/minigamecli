#ifndef WEBCLIWINDOW_H
#define WEBCLIWINDOW_H

#include "ICliWindow.h"
#include <QWebEngineView>
#include <QProgressBar>
#include <QLabel>

struct WebCliWindowUI
{
    QWidget *centralwidget;
    QWidget *webViewCtn;
    QProgressBar *progressBar;
    QLabel *textStatus;
};

class WebCliWindow : public ICliWindow
{
    Q_OBJECT
public:
    using ICliWindow::ICliWindow;
    ~WebCliWindow();

    void gotoURL(const QString &url, const QString &watchUrl) override;
    void setProgressTip(const QString &tip) override;
    void setProgressValue(qint64 bytesReceived, qint64 bytesTotal) override;

protected:
    QWebEngineView *m_webView = nullptr;
    QString m_watchUrl;

    void setUI(WebCliWindowUI *ui, bool showWebView);

private:
    WebCliWindowUI *m_ui;

private slots:
    void onUrlChanged(const QUrl &url);
};

#endif // WEBCLIWINDOW_H
