QT       += core gui
QT       += webenginewidgets
QT       += core5compat

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

QMAKE_CFLAGS_RELEASE = /MT
QMAKE_CXXFLAGS_RELEASE = /MT
QMAKE_CFLAGS_DEBUG = /MTd
QMAKE_CXXFLAGS_DEBUG = /MTd

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    cfgmgr.cpp \
    downloadmanager.cpp \
    gamelogic.cpp \
    log.cpp \
    main.cpp \
    qos.cpp \
    uploader.cpp \
    webcliwindow.cpp

HEADERS += \
    ICliWindow.h \
    cfgmgr.h \
    downloadmanager.h \
    gamelogic.h \
    include/Macros.h \
    log.h \
    qos.h \
    uploader.h \
    utils.h \
    webcliwindow.h

LIBS += -lDbgHelp
CONFIG(debug, debug|release) {
    LIBS += -L$$PWD/lib -lquazipd
} else {
    LIBS += -L$$PWD/lib -lquazip
}

# Windows.h
LIBS += -luser32

INCLUDEPATH += include/ \
    $$system_path($$[QT_INSTALL_PREFIX])\include\QtZlib

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

# 根据不同平台make时包含不同代码和资源集
# 开启以下宏用于编译对应平台的微端
DEFINES += GM_PLAT_ZTJ_TOP1GAME  # 诛天记台服

if(contains(DEFINES, GM_PLAT_ZTJ_TOP1GAME)) {
    # 诛天记台服
    SOURCES += ztj_top1game_cliwindowsimple.cpp
    HEADERS += \
        include/ztj_top1game.h \
        ztj_top1game_cliwindowsimple.h
    RESOURCES += ztj_top1game.qrc
    FORMS += ztj_top1game_cliwindowSimple.ui
    RC_FILE = res/ztj_top1game/djwsstandalone.rc
    TARGET = djwsstandalone
} else {
    # 默认
    SOURCES += cliwindow.cpp
    HEADERS += \
        include/defaultlang.h \
        cliwindow.h
    RESOURCES += myRes.qrc
    FORMS += cliwindow.ui
    RC_ICONS = res/icon_64.ico
}
