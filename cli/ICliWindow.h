#ifndef ICLIWINDOW_H
#define ICLIWINDOW_H

#include <QString>
#include <QMainWindow>

class ICliWindow : public QMainWindow
{
    Q_OBJECT
public:
    using QMainWindow::QMainWindow;
    virtual void gotoURL(const QString &url, const QString &watchUrl) = 0;
    virtual void setProgressTip(const QString &tip) = 0;
    virtual void setProgressValue(qint64 bytesReceived, qint64 bytesTotal) = 0;

signals:
    void onEnterGame(const QString &query);
};

#endif // ICLIWINDOW_H
