#include "qos.h"
#include <QDateTime>
#include <QNetworkRequest>

Qos::Qos()
{
}

void Qos::setQosEnv(const QString &url, const QString &appVer, const QString &osVer)
{
    m_url = url;
    m_appVer = appVer;
    m_osVer = osVer;
    m_startTime = QDateTime::currentSecsSinceEpoch();
    m_mgr = new QNetworkAccessManager();
}

void Qos::setUID(const QString &uid)
{
    m_uid = uid;
}

int Qos::getStartTime() const
{
    return m_startTime;
}

void Qos::report(const QosType &type, const char *data)
{
    if(m_url.isNull())
    {
        return;
    }
    auto now = QDateTime::currentSecsSinceEpoch();
    auto timeDelta = now - m_startTime;
    int svrId = 0;
    auto logStr = QString::asprintf("FY={G:1;S:%lld;W:%d;U:0;N:%s;T:%d;%s;AGENT:;OS:%s;J:%lld;V:%s;}",
                                    m_startTime,
                                    svrId,
                                    m_uid.toStdString().c_str(),
                                    type,
                                    data,
                                    m_osVer.toStdString().c_str(),
                                    timeDelta,
                                    m_appVer.toStdString().c_str());
    QString u = m_url + "?data=" + logStr;

    qInfo("request qos: %s", u.toStdString().c_str());

    QNetworkRequest req;
    req.setUrl(QUrl(u));
    m_mgr->get(req);
}
