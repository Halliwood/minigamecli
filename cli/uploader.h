#ifndef UPLOADER_H
#define UPLOADER_H

#include <QObject>
#include <QNetworkAccessManager>

class Uploader : public QObject
{
    Q_OBJECT
public:
    explicit Uploader(QObject *parent = nullptr);

    void setUp(const QString &userName);
    bool upload(const QString &filePath, const QString &url, const QString &fileName, const QString &formName);

private:
    QNetworkAccessManager m_mgr;
    QString m_userName;

signals:

};

#endif // UPLOADER_H
