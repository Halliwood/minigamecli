// wxWidgets "Hello World" Program
// For compilers that support precompilation, includes "wx/wx.h".
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "wxSkinXFrame.h"

class MyApp : public wxApp
{
public:
    virtual bool OnInit();
};
wxIMPLEMENT_APP(MyApp);
bool MyApp::OnInit()
{
    wxInitAllImageHandlers();

    wxSkinXFrame* f = new wxSkinXFrame(NULL, wxID_ANY);
    //���ô���Ƥ��
    f->SetNormalImage(wxImage("bgShape.png"));
    f->SetShapeImage(wxImage("bgShape.png"));
    f->SetOverImage(wxImage("bgShape.png"));
    f->SetDisableImage(wxImage("bgShape.png"));
    f->SetSize(1200, 700);
    f->Center();

    f->Show(true);

    return true;
}